<?php

	// HIDE PORTFOLIO FROM CLIENT ADMIN
	function custom_remove_menus(){
	    $current_user = wp_get_current_user();
	    $user_id = $current_user->ID;

	    // Check user's roles
	    $user = new WP_User( $user_id );
	    if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
	        if( in_array( 'editor', $user->roles ) ) {
	            // Remove menu items
	            remove_menu_page( 'edit.php?post_type=portfolio');
	        }
	    }
	}
	add_action( 'admin_menu', 'custom_remove_menus' );

	// ADD CLASSES
	add_theme_support('avia_template_builder_custom_css');